﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class SaveTest : MonoBehaviour
{

    private GameObject[] objetos;
    List<BoxTest> boxes = new List<BoxTest>();

    private string rutaArchivoDatos = "/Data/data.json";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) {
            boxes.Clear();
            ObtenerObjetos();
            ObtenerPosiciones();
            ObtenerTamañoLista();
            GuardarDatos(boxes);
        }
    }

    private void ObtenerObjetos() {
        objetos = GameObject.FindGameObjectsWithTag("box");
    }

    private void ObtenerPosiciones()
    {
        foreach (GameObject objeto in objetos) {

            BoxData boxData = objeto.GetComponent<BoxData>();
            string tipoObjeto = boxData.RetornarTipoObjeto();
            float posicionX = boxData.RetornarPosicionX();
            float posicionY = boxData.RetornarPosicionY();
            float posicionZ = boxData.RetornarPosicionZ();

            //print(tipoObjeto + "/" + posicionX + "/" + posicionY + "/" + posicionZ);

            boxes.Add(new BoxTest(tipoObjeto, posicionX, posicionY, posicionZ));
            
        }
    }

    private int ObtenerTamañoLista()
    {
        int tamañoLista = 0;
        tamañoLista = boxes.Count;
        Debug.Log(tamañoLista);
        return tamañoLista;
        

    }

    private void GuardarDatos(List<BoxTest>boxes) {
        foreach (BoxTest box in boxes)
        {
            string datosEnJason = JsonUtility.ToJson(box);
            string rutaArchivo = Application.dataPath + rutaArchivoDatos;
            File.WriteAllText(rutaArchivo, datosEnJason);

        }
        
    }
}
