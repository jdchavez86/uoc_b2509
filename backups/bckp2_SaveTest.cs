﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTest : MonoBehaviour
{

    private GameObject[] objetos;
    List<BoxTest> boxes = new List<BoxTest>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) {
            boxes.Clear();
            ObtenerObjetos();
            ObtenerPosiciones();
            ObtenerTamañoLista();
        }
    }

    private void ObtenerObjetos() {
        objetos = GameObject.FindGameObjectsWithTag("box");
    }

    private void ObtenerPosiciones()
    {
        foreach (GameObject objeto in objetos) {

            BoxData boxData = objeto.GetComponent<BoxData>();
            string tipoObjeto = boxData.RetornarTipoObjeto();
            float posicionX = boxData.RetornarPosicionX();
            float posicionY = boxData.RetornarPosicionY();
            float posicionZ = boxData.RetornarPosicionZ();

            //print(tipoObjeto + "/" + posicionX + "/" + posicionY + "/" + posicionZ);

            boxes.Add(new BoxTest(tipoObjeto, posicionX, posicionY, posicionZ));

            
            //foreach (BoxTest box in boxes)
            //{
            //    print(tipoObjeto + "/" + posicionX + "/" + posicionY + "/" + posicionZ);
            //}
        }
    
    }

   private int ObtenerTamañoLista()
    {
        int tamañoLista = 0;
        tamañoLista = boxes.Count;
        Debug.Log(tamañoLista);
        return tamañoLista;

    }
}
