﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTest : MonoBehaviour
{

    private GameObject[] objetos;
    List<BoxTest> boxes = new List<BoxTest>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) {
            ObtenerObjetos();
            ObtenerPosiciones();
            LlenarLista();
        };
    }

    private void ObtenerObjetos() {
        objetos = GameObject.FindGameObjectsWithTag("box");
    }

    private void ObtenerPosiciones()
    {
        for (int i = 0;i < objetos.Length; i++)
        {
            Vector3 posicion = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
            Debug.Log(posicion);
            
        }
    }

    private void LlenarLista()
    {
        boxes.Clear();
    
        foreach (GameObject objeto in objetos)
        {
            BoxData boxData = FindObjectOfType<BoxData>();
            string tipoPrefab = boxData.boxTest.tipoObjeto;
            float posicionX = boxData.boxTest.posicionX;
            float posicionY = boxData.boxTest.posicionY;
            float posicionZ = boxData.boxTest.posicionZ;
            boxes.Add(new BoxTest(tipoPrefab, posicionX, posicionY, posicionZ));
       
        }

        foreach (BoxTest box in boxes)
        {
            print(box.tipoObjeto + "-" + box.posicionX + "-" + box.posicionY + "-" + box.posicionZ);
        }
    }
}
