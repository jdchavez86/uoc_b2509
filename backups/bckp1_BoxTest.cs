﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoxTest 
{
    public string tipoObjeto;
    public float posicionX;
    public float posicionY;
    public float posicionZ;

    public BoxTest(string newTipoObjeto, float newPosicionX, float newPosicionY, float newPosicionZ) { // creo que esto es el constructor... debe definirse al crear clases
        tipoObjeto = newTipoObjeto;
        posicionX = newPosicionX;
        posicionY = newPosicionY;
        posicionZ = newPosicionZ;
    }
}


