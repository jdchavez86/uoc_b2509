﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


public class SaveTest : MonoBehaviour
{

    private GameObject[] objetos;
    public Transform caja;
    public List<BoxTest> boxes = new List<BoxTest>();
        

    private string rutaArchivoDatos = "/Data/data.json";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) {
            boxes.Clear();
            ObtenerObjetos();
            ObtenerPosiciones();
            ObtenerTamañoLista();
            GuardarDatos();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            CargarDatos();
        }
    }

    private void ObtenerObjetos() {
        objetos = GameObject.FindGameObjectsWithTag("box");
    }

    private void ObtenerPosiciones()
    {
        foreach (GameObject objeto in objetos) {

            BoxData boxData = objeto.GetComponent<BoxData>();
            string tipoObjeto = boxData.RetornarTipoObjeto();
            float posicionX = boxData.RetornarPosicionX();
            float posicionY = boxData.RetornarPosicionY();
            float posicionZ = boxData.RetornarPosicionZ();

            //print(tipoObjeto + "/" + posicionX + "/" + posicionY + "/" + posicionZ);

            boxes.Add(new BoxTest(tipoObjeto, posicionX, posicionY, posicionZ));
            
        }
    }

    private int ObtenerTamañoLista()
    {
        int tamañoLista = 0;
        tamañoLista = boxes.Count;
        Debug.Log(tamañoLista);
        return tamañoLista;
        

    }

  
    private void GuardarDatos()
    {

        Wrapper wrapper = new Wrapper(boxes);
        string datosEnJason = JsonUtility.ToJson(wrapper, true);
        string rutaArchivo = Application.dataPath + rutaArchivoDatos;
        File.WriteAllText(rutaArchivo, datosEnJason);
                

    }

    private void CargarDatos() {

        string rutaArchivo = Application.dataPath + rutaArchivoDatos;
        string datosEnJson = File.ReadAllText(rutaArchivo);
        Wrapper wrapper = JsonUtility.FromJson<Wrapper>(datosEnJson);
        for (int i = 0; i < wrapper.boxesList.Count; i++)
        {
            Debug.Log(wrapper.boxesList[i].tipoObjeto + "/" + wrapper.boxesList[i].posicionX + "/" + wrapper.boxesList[i].posicionY + "/" + wrapper.boxesList[i].posicionZ);
            if (wrapper.boxesList[i].tipoObjeto == "caja")
            {
                //float floatPosicionX = float.Parse(wrapper.boxesList[i].posicionX);
                Instantiate(caja, new Vector3(wrapper.boxesList[i].posicionX, wrapper.boxesList[i].posicionY, wrapper.boxesList[i].posicionZ), caja.rotation);
            }
        }
   
    }

}
