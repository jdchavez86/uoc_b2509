﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetData : MonoBehaviour
{

    public TargetClass targetClass;
  

    // Start is called before the first frame update
    void Start()
    {

        targetClass = new TargetClass(targetClass.tipoObjeto,targetClass.posicionX,targetClass.posicionY,targetClass.posicionZ);
        targetClass.tipoObjeto = "target"; //este sí lo dejaré en inglés.
        targetClass.posicionX = gameObject.transform.position.x;
        targetClass.posicionY = gameObject.transform.position.y;
        targetClass.posicionZ = gameObject.transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string RetornarTipoObjeto() {
        return targetClass.tipoObjeto;
    }

    public float RetornarPosicionX()
    {
        return targetClass.posicionX;
    }

    public float RetornarPosicionY()
    {
        return targetClass.posicionY;
    }

    public float RetornarPosicionZ()
    {
        return targetClass.posicionZ;
    }
}
