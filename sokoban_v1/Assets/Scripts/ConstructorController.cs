﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructorController : MonoBehaviour
{
    public GameObject erraser;
    public GameObject wallBrush;
    public GameObject boxBrush;
    public GameObject targetBrush;
    public GameObject playerSpawnPointBrush;
    public GameObject boxPrefab;
    public GameObject wallPrefab;
    public GameObject targetPrefab;
    public GameObject playerSpawnPoint;
    private GameObject objetoEnCasilla; //este es el objeto que está ocupando determinado espacio, se detecta con colisión.
    private Vector3 posiciónActual; // guardarè cada posición
    private List<Vector3> listaPosiciones; //esta lista la utilizaré para retornar el pincel a la superficie pintable cuando se salga de ella.

    private int idPincel = 0;
    private string pincelActivo;
    private bool construcción; // variable para definir si se puede construir o no.
    private bool existenciaObjeto; //variable para saber definir si existe objeto en determinado espacio, con el fin de destruir y construir ahí.
    bool existenciaSpawnPoint;

    float intervaloTiempo;
    private float gap = 0.1f;
    

    // Start is called before the first frame update
    void Start()
    {
        CambiarPincel(); // cambia el pincel por primera vez
        listaPosiciones = new List<Vector3>();

    }

    // Update is called once per frame
    void Update()
    {
        intervaloTiempo += Time.deltaTime;

        //if (intervaloTiempo >= gap)
        //{
            MoverCursor();
            //intervaloTiempo = 0;
        //}

        ValidarSiConstruir();
                       
        if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            CambiarPincel();
        }
        
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
        {
            ConstruirObjeto();
        }
        
        if (pincelActivo == "erraser" && existenciaObjeto == true && Input.GetKeyDown(KeyCode.Return))
        {
            DestruirObjeto();
        }

    }

    private void MoverCursor() {
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (intervaloTiempo >= gap) { 
                transform.position += Vector3.forward;
                posiciónActual = gameObject.transform.position;
                listaPosiciones.Add(posiciónActual);
                intervaloTiempo = 0;
            }
        }

        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (intervaloTiempo >= gap)
            {
                transform.position += Vector3.back;
                posiciónActual = gameObject.transform.position;
                listaPosiciones.Add(posiciónActual);
                intervaloTiempo = 0;
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (intervaloTiempo >= gap) {
                transform.position += Vector3.left;
                posiciónActual = gameObject.transform.position;
                listaPosiciones.Add(posiciónActual);
                intervaloTiempo = 0;
            }
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (intervaloTiempo >= gap)
            {
                transform.position += Vector3.right;
                posiciónActual = gameObject.transform.position;
                listaPosiciones.Add(posiciónActual);
                intervaloTiempo = 0;
            }
        }
    }

    private void CambiarPincel()
    {
        switch (idPincel)
        {
            case 0: //borrador
                idPincel = 1;
                erraser.SetActive(true);
                wallBrush.SetActive(false);
                boxBrush.SetActive(false);
                targetBrush.SetActive(false);
                playerSpawnPointBrush.SetActive(false);
                pincelActivo = "erraser";
                break;

            case 1: // pared
                idPincel = 2;
                erraser.SetActive(false);
                wallBrush.SetActive(true);
                boxBrush.SetActive(false);
                targetBrush.SetActive(false);
                playerSpawnPointBrush.SetActive(false);
                pincelActivo = "wallBrush";
                break;

            case 2: // caja
                idPincel = 3;
                erraser.SetActive(false);
                wallBrush.SetActive(false);
                boxBrush.SetActive(true);
                targetBrush.SetActive(false);
                playerSpawnPointBrush.SetActive(false);
                pincelActivo = "boxBrush";
                break;

            case 3: // objetivo
                idPincel = 4;
                erraser.SetActive(false);
                wallBrush.SetActive(false);
                boxBrush.SetActive(false);
                targetBrush.SetActive(true);
                playerSpawnPointBrush.SetActive(false);
                pincelActivo = "targetBrush";
                break;

            case 4: // playerSpawnPoint
                idPincel = 0;
                erraser.SetActive(false);
                wallBrush.SetActive(false);
                boxBrush.SetActive(false);
                targetBrush.SetActive(false);
                playerSpawnPointBrush.SetActive(true);
                pincelActivo = "playerSpawnPointBrush";
                break;
        }
    }

    private bool ValidarSiConstruir() // método para evaluar si el pincel se encuentra en un área donde se puede construir.
    {

        if (transform.position != playerSpawnPoint.gameObject.transform.position)
        {
            existenciaSpawnPoint = false;
        }
        else { existenciaSpawnPoint = true; }
   
        RaycastHit hit; //esto guarda la información que produzca el rayo al chocar.
        Ray rayoPiso = new Ray(transform.position, Vector3.down * 1f); // este es el rayo que se lanza.
        Debug.DrawRay(transform.position, Vector3.down * 1f, Color.red); // simplemente dibuja un rayo con el color, distancia y longitud especificado.
            
        if (Physics.Raycast(rayoPiso, out hit, 1f))
        { // si el rayo choca con algo, entre al if.

            if (hit.collider.name == "Base") // solamente entra si el rayo colisiona con objeto que tiene el nombre especìfico.
            {
                construcción = true;
            }
        }

        if (hit.collider == null || existenciaSpawnPoint == true) // entra comparación se realiza si el rayo no ha atravesado ninguna colisión o si en el lugar existe spawn poin
        {
            construcción = false; 
        }

        if(hit.collider == null)
        {
            //Esto permite no salirse del área construible. El pincel vuelve a la penúltima posición si no hay suelo.
            transform.position = listaPosiciones[listaPosiciones.Count - 2];
            listaPosiciones.Add(transform.position);

        }

        //Debug.Log(construcción);

        return construcción;
    }

    private void OnTriggerEnter(Collider other) // lo utilizaré para saber si existe un objeto en el lugar, y así poder destruirlo
    {
        existenciaObjeto = true;
        Debug.Log(existenciaObjeto);
        objetoEnCasilla = other.gameObject;
        Debug.Log(objetoEnCasilla.name);

    }

    private void OnTriggerExit(Collider other) // a salir del trigger  toca resetear, porque de lo contrario se podría borrar una celda que no está siendo tocada.
    {
        existenciaObjeto = false;
        Debug.Log(existenciaObjeto);
        objetoEnCasilla = null;
        //Debug.Log(objetoEnCasilla.name);
    }

    private void ConstruirObjeto()
    {
        
        if (construcción == true)
        {
            if (existenciaObjeto == true)
            {
                DestruirObjeto();
            }

            switch (pincelActivo)
            {
                case "boxBrush":
                    Instantiate(boxPrefab, transform.position, transform.rotation);
                    break;

                case "wallBrush":
                    Instantiate(wallPrefab, transform.position, transform.rotation);
                    break;

                case "targetBrush":
                    Instantiate(targetPrefab, transform.position, transform.rotation);
                    break;

                case "playerSpawnPointBrush":
                    playerSpawnPoint.gameObject.transform.position = transform.position;
                    break;
            }
        }
    }

    private void DestruirObjeto()
    {
        Destroy(objetoEnCasilla);
    }

    private void RegresarASuperficie()
    {

    }

    }



