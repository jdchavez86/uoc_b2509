﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wrapper {

    public List<BoxClass> boxesList;
    public List<WallClass> wallsList;
    public List<TargetClass> targetsList;
    public DataManager.SpawnPointClass spawnPoint;

    
    public Wrapper(List<BoxClass> _boxesList, List<WallClass> _wallsList, List<TargetClass> _targetsList,DataManager.SpawnPointClass _spawnPoint)
    {
        boxesList = _boxesList;
        wallsList = _wallsList;
        targetsList = _targetsList;
        spawnPoint = _spawnPoint;
    }

}
