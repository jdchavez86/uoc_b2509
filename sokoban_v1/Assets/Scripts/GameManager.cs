﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject menu;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetKeyDown(KeyCode.Escape))
        {
            ActivarDesactivarMenú();
        } 

       if (Input.GetKeyDown(KeyCode.F2))
        {
            IrAEditor();
        }

       if (Input.GetKeyDown(KeyCode.F1)){
            RegresarMenú();
        }
    }

    public void ActivarDesactivarMenú()
    {
        menu.gameObject.SetActive(!menu.gameObject.activeSelf);
    }


    public void RegresarMenú()
    {
        SceneManager.LoadScene("principal");
    }

    public void Empezar()
    {
        SceneManager.LoadScene("juego_v1");
    }

    public void Salir()
    {
        Debug.Log("Saliste del juego");
        Application.Quit();
    }

    public void IrAEditor()
    {
        SceneManager.LoadScene("editor-niveles_v1");
    }


}

