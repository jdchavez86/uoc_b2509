﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxData : MonoBehaviour
{

    public BoxClass boxClass;
  

    // Start is called before the first frame update
    void Start()
    {

        boxClass = new BoxClass(boxClass.tipoObjeto,boxClass.posicionX,boxClass.posicionY,boxClass.posicionZ);
        boxClass.tipoObjeto = "caja";
        boxClass.posicionX = gameObject.transform.position.x;
        boxClass.posicionY = gameObject.transform.position.y;
        boxClass.posicionZ = gameObject.transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string RetornarTipoObjeto() {
        return boxClass.tipoObjeto;
    }

    public float RetornarPosicionX()
    {
        return boxClass.posicionX;
    }

    public float RetornarPosicionY()
    {
        return boxClass.posicionY;
    }

    public float RetornarPosicionZ()
    {
        return boxClass.posicionZ;
    }
}
