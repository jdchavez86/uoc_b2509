﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEditor;


public class DataManager : MonoBehaviour
{

    public SpawnPointClass spawnPointClass;
    private GameObject[] cajas;
    private GameObject[] paredes;
    private GameObject[] targets;
    public Transform caja;
    public Transform pared;
    public Transform target;
    public Transform spawnPoint; // en el inspector se llama spawn_point
    private Vector3 posicionInicialSpawnPoint; // la necesito para que al reiniciar nivel vuelva todo a su lugar.
    public List<BoxClass> listaCajas = new List<BoxClass>();
    public List<WallClass> listaParedes = new List<WallClass>();
    public List<TargetClass> listaTargets = new List<TargetClass>();
    public InputField nombreArchivoCampoTexto;
    public Button botonGrabar;
    public Button botonCargar;
    public GameObject panelAlertas;
    public Text campoTextoAlertas;
        
    private string rutaArchivoDatos = "/Data/";
    private string nombreArchivo;
    private bool suficienciaCajas;
    private bool suficienciaTargets;

    

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Application.persistentDataPath);
        //campoTextoAlertas.text = Application.dataPath; Tuve que usar esta opción para saber dónde poner los archivos del juego después de construir el build

        spawnPointClass = new SpawnPointClass
        {
            referenciaSpawnPoint = "spawnPoint",
            spawnPointX = spawnPoint.position.x,
            spawnPointY = spawnPoint.position.y,
            spawnPointZ = spawnPoint.position.z
        };

        posicionInicialSpawnPoint = spawnPoint.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
        ValidarExistenciaNombreArchivo(nombreArchivoCampoTexto);
    }

    public void ValidarExistenciaNombreArchivo(InputField input) // método que válida existencia de texto en inputfield y asigna nombre a archivo para cargar o guardar.
    {

        if (input.text.Length > 0)
        {
            nombreArchivo = nombreArchivoCampoTexto.text + ".json";
            botonCargar.interactable = true;
            botonGrabar.interactable = true;
        }
        else
        {
            botonCargar.interactable = false;
            botonGrabar.interactable = false;
        }
    }

    public void Guardar()
    {
        listaCajas.Clear();
        listaParedes.Clear();
        listaTargets.Clear();
        ObtenerObjetos();
        ObtenerPosiciones();
        ValidarCajasYTargets();
        GuardarDatos();
    }

       
    private void ObtenerObjetos() {
        cajas = GameObject.FindGameObjectsWithTag("box");
        paredes = GameObject.FindGameObjectsWithTag("wall");
        targets = GameObject.FindGameObjectsWithTag("target");
    }

    private void ObtenerPosiciones()
    {
        foreach (GameObject caja in cajas) {

            BoxData boxData = caja.GetComponent<BoxData>();
            string tipoObjeto = boxData.RetornarTipoObjeto();
            float posicionX = boxData.RetornarPosicionX();
            float posicionY = boxData.RetornarPosicionY();
            float posicionZ = boxData.RetornarPosicionZ();

            //print(tipoObjeto + "/" + posicionX + "/" + posicionY + "/" + posicionZ);

            listaCajas.Add(new BoxClass(tipoObjeto, posicionX, posicionY, posicionZ));
            
        }

        foreach (GameObject pared in paredes)
        {

            WallData wallData = pared.GetComponent<WallData>();
            string tipoObjeto = wallData.RetornarTipoObjeto();
            float posicionX = wallData.RetornarPosicionX();
            float posicionY = wallData.RetornarPosicionY();
            float posicionZ = wallData.RetornarPosicionZ();

            listaParedes.Add(new WallClass(tipoObjeto, posicionX, posicionY, posicionZ));

        }

        foreach (GameObject target in targets)
        {

            TargetData targetData = target.GetComponent<TargetData>();
            string tipoObjeto = targetData.RetornarTipoObjeto();
            float posicionX = targetData.RetornarPosicionX();
            float posicionY = targetData.RetornarPosicionY();
            float posicionZ = targetData.RetornarPosicionZ();

            listaTargets.Add(new TargetClass(tipoObjeto, posicionX, posicionY, posicionZ));

        }

        ///// actualizar posición del spawnPoint en el momento de guardar.
        spawnPointClass.spawnPointX = spawnPoint.position.x;
        spawnPointClass.spawnPointY = spawnPoint.position.y;
        spawnPointClass.spawnPointZ = spawnPoint.position.z;

    }

    public void ValidarCajasYTargets() // condiciones para  asegurar la calidad mínima del nivel creado antes de guardar.
    {
        if (listaTargets.Count > listaCajas.Count)
        {
            campoTextoAlertas.text = "Faltan cajas. Antes de guardar, asegúrese de que existen suficientes cajas para cada punto de llegada (target)";
            ActivarDesactivarAlerta();
            suficienciaCajas = false;
            
        }
        if (listaTargets.Count <= listaCajas.Count)
        {
            suficienciaCajas = true;
        }

        if (listaTargets.Count == 0)
        {
            suficienciaTargets = false;
            campoTextoAlertas.text = "Faltan targets. Para guardar, añada un punto de llegada (target)";
            ActivarDesactivarAlerta();

        }
        else {
            suficienciaTargets = true;
        }

    }
             
    public void GuardarDatos()
    {
        if (suficienciaCajas == true && suficienciaTargets == true)
        {
            Wrapper wrapper = new Wrapper(listaCajas, listaParedes, listaTargets, spawnPointClass);
            string datosEnJason = JsonUtility.ToJson(wrapper, true);
            string rutaArchivo = Application.dataPath + rutaArchivoDatos + nombreArchivo;
            File.WriteAllText(rutaArchivo, datosEnJason);
            campoTextoAlertas.text = "Grabación exitosa. El nivel ha sido guardado correctamente como '" + nombreArchivo +"'";
            ActivarDesactivarAlerta();
            
        }
    }

    public void ReiniciarNivel() // método que se necesita para que una construcción en curso no se sobreponga a construcción cargada
    {
        ObtenerObjetos();
        foreach (GameObject caja in cajas)
        {
            Destroy(caja);
        }
        foreach (GameObject pared in paredes)
        {
            Destroy(pared);
        }
        foreach (GameObject target in targets)
        {
            Destroy(target);
        }

        spawnPoint.transform.position = posicionInicialSpawnPoint;
    }

    public void CargarDatos() {

        string rutaArchivo = Application.dataPath + rutaArchivoDatos+nombreArchivo;
        if (File.Exists(rutaArchivo))
        {
            ReiniciarNivel();
            string datosEnJson = File.ReadAllText(rutaArchivo);
            Wrapper wrapper = JsonUtility.FromJson<Wrapper>(datosEnJson);
            for (int i = 0; i < wrapper.boxesList.Count; i++)
            {
                if (wrapper.boxesList[i].tipoObjeto == "caja")
                {
                    Instantiate(caja, new Vector3(wrapper.boxesList[i].posicionX, wrapper.boxesList[i].posicionY, wrapper.boxesList[i].posicionZ), caja.rotation);
                }
            }

            for (int i = 0; i < wrapper.wallsList.Count; i++)
            {
                if (wrapper.wallsList[i].tipoObjeto == "pared")
                {
                    Instantiate(pared, new Vector3(wrapper.wallsList[i].posicionX, wrapper.wallsList[i].posicionY, wrapper.wallsList[i].posicionZ), pared.rotation);
                }
            }

            for (int i = 0; i < wrapper.targetsList.Count; i++)
            {
                if (wrapper.targetsList[i].tipoObjeto == "target")
                {
                    Instantiate(target, new Vector3(wrapper.targetsList[i].posicionX, wrapper.targetsList[i].posicionY, wrapper.targetsList[i].posicionZ), target.rotation);
                }
            }

            spawnPoint.transform.position = new Vector3(wrapper.spawnPoint.spawnPointX, wrapper.spawnPoint.spawnPointY, wrapper.spawnPoint.spawnPointZ);
        }
        else
        {
            campoTextoAlertas.text= "Archivo de nivel no encontrado. No existe el nivel indicado, escriba correctamente el nombre de un nivel previamente guardado.";
            ActivarDesactivarAlerta();
            Debug.Log("no existe archivo");
        }
        
    }

    public void ActivarDesactivarAlerta()
    {
        panelAlertas.gameObject.SetActive(!panelAlertas.gameObject.activeSelf);
    }
    
    [System.Serializable]
    public class SpawnPointClass
    {
        public string referenciaSpawnPoint;
        public float spawnPointX;
        public float spawnPointY;
        public float spawnPointZ;
    }
}
