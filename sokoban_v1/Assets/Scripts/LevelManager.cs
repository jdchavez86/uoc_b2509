﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    // Start is called before the first frame update

    private GameObject[] cajas;
    private GameObject[] paredes;
    private GameObject[] targets;
    public GameObject playerPrefab;
    public GameObject cajaPrefab;
    public GameObject paredPrefab;
    public GameObject targetPrefab;
    public GameObject grupoObjetos;
    public Text textoNivel;
    private string rutaArchivoDatos;
    private string nombreArchivoNivelActual;
    private string[] listaNiveles;
    public string[] listaArchivosNiveles;
    public int indiceNivel;
    public int sumatoriaPuntosNivel;
    private int targetsEnNivel;


    void Start()
    {
        grupoObjetos = GameObject.FindGameObjectWithTag("grupo");

        indiceNivel = 0;

        rutaArchivoDatos = "/Data/";

        listaArchivosNiveles = new string[10];
        listaArchivosNiveles[0] = "nivel1.json";
        listaArchivosNiveles[1] = "nivel2.json";
        listaArchivosNiveles[2] = "nivel3.json";
        listaArchivosNiveles[3] = "nivel4.json";
        listaArchivosNiveles[4] = "nivel5.json";
        listaArchivosNiveles[5] = "nivel6.json";
        listaArchivosNiveles[6] = "nivel7.json";
        listaArchivosNiveles[7] = "nivel8.json";
        listaArchivosNiveles[8] = "nivel9.json";
        listaArchivosNiveles[9] = "nivel10.json";


        listaNiveles = new string[10];
        listaNiveles[0] = "Nivel 1";
        listaNiveles[1] = "Nivel 2";
        listaNiveles[2] = "Nivel 3";
        listaNiveles[3] = "Nivel 4";
        listaNiveles[4] = "Nivel 5";
        listaNiveles[5] = "Nivel 6";
        listaNiveles[6] = "Nivel 7";
        listaNiveles[7] = "Nivel 8";
        listaNiveles[8] = "Nivel 9";
        listaNiveles[9] = "Nivel 10";

        nombreArchivoNivelActual = listaArchivosNiveles[indiceNivel];
        

        CargarDatos();
    }


    // Update is called once per frame
    void Update()
    {

    }


    private void ObtenerObjetos()
    {
        cajas = GameObject.FindGameObjectsWithTag("box");
        paredes = GameObject.FindGameObjectsWithTag("wall");
        targets = GameObject.FindGameObjectsWithTag("target");
    }

    public void ReiniciarNivel() // método que se necesita para que una construcción en curso no se sobreponga a construcción cargada
    {
        ObtenerObjetos();

        foreach (GameObject caja in cajas)
        {
            Destroy(caja);
        }
        foreach (GameObject pared in paredes)
        {
            Destroy(pared);
        }
        foreach (GameObject target in targets)
        {
            Destroy(target);
        }
        
    }

    public void CargarDatos()
    {

        string rutaArchivo = Application.dataPath + rutaArchivoDatos + nombreArchivoNivelActual;
        if (File.Exists(rutaArchivo))
        {
            sumatoriaPuntosNivel = 0;
            targetsEnNivel = 0;
            ReiniciarNivel();
            MostrarNivel();
            string datosEnJson = File.ReadAllText(rutaArchivo);
            Wrapper wrapper = JsonUtility.FromJson<Wrapper>(datosEnJson);
            for (int i = 0; i < wrapper.boxesList.Count; i++)
            {
                if (wrapper.boxesList[i].tipoObjeto == "caja")
                {
                    GameObject nuevoObjeto = Instantiate(cajaPrefab, new Vector3(wrapper.boxesList[i].posicionX, wrapper.boxesList[i].posicionY, wrapper.boxesList[i].posicionZ), cajaPrefab.transform.rotation);
                    nuevoObjeto.transform.parent = grupoObjetos.transform;
                }
            }

            for (int i = 0; i < wrapper.wallsList.Count; i++)
            {
                if (wrapper.wallsList[i].tipoObjeto == "pared")
                {
                    GameObject nuevoObjeto = Instantiate(paredPrefab, new Vector3(wrapper.wallsList[i].posicionX, wrapper.wallsList[i].posicionY, wrapper.wallsList[i].posicionZ), paredPrefab.transform.rotation);
                    nuevoObjeto.transform.parent = grupoObjetos.transform;
                }
            }

            for (int i = 0; i < wrapper.targetsList.Count; i++)
            {
                if (wrapper.targetsList[i].tipoObjeto == "target")
                {
                    GameObject nuevoObjeto = Instantiate(targetPrefab, new Vector3(wrapper.targetsList[i].posicionX, wrapper.targetsList[i].posicionY, wrapper.targetsList[i].posicionZ), targetPrefab.transform.rotation);
                    nuevoObjeto.transform.parent = grupoObjetos.transform;
                    targetsEnNivel++;
                }
            }

            playerPrefab.transform.position = new Vector3(wrapper.spawnPoint.spawnPointX, wrapper.spawnPoint.spawnPointY, wrapper.spawnPoint.spawnPointZ);
                        
        }
        else
        {
            Debug.Log("no existe archivo");
        }
    }

    public void ValidarVictoriaNivel()
    {
        if (sumatoriaPuntosNivel == targetsEnNivel)
        {
            indiceNivel++;
            //            targetsEnNivel = 0;
            if (indiceNivel == 10)
            {
                FinalizarJuego();
            }
            else
            {
                nombreArchivoNivelActual = listaArchivosNiveles[indiceNivel];
                CargarDatos();
            }
        }
    }

    private void MostrarNivel()
    {
        textoNivel.text = listaNiveles[indiceNivel];
    }

    private void FinalizarJuego()
    {
        Debug.Log("Fin del juego");
        SceneManager.LoadScene("principal");
    }
}
