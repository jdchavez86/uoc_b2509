﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private LevelManager levelManager;
    public float velocidadMovimiento;
       
    // Start is called before the first frame update
    void Start()
    {

        levelManager = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * velocidadMovimiento, 0f, Input.GetAxis("Vertical") * Time.deltaTime * velocidadMovimiento);
        
    }

    private void OnCollisionEnter(Collision collision) // por si alguien le da por crear un nivel que no tiene paredes y el player y las cajas pueden caer...
    {
        if (collision.gameObject.name == "Fondo")
        {
            levelManager.CargarDatos();
        }
    }
}
