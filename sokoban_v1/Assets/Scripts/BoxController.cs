﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{

    public float distanciaDetección;
    private Rigidbody rigidbodyCaja;
    private LevelManager levelManager;

    // Start is called before the first frame update
    void Start()
    {
        rigidbodyCaja = GetComponent<Rigidbody>();
        levelManager = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
        ValidarMovimientoZ();
        ValidarMovimientoX();
    }

    public void ValidarMovimientoZ()
    {
        RaycastHit hit; // solamente se necesita un hit para guardar toda la información del raycast... cada rayo parece que queda serializado
       
        Ray rayoFrontal = new Ray(transform.position, Vector3.forward);
        Ray rayoTrasero = new Ray(transform.position, Vector3.back);
        

        Debug.DrawRay(transform.position, Vector3.forward * distanciaDetección,Color.red);
        Debug.DrawRay(transform.position, Vector3.back* distanciaDetección, Color.red);
       

        if (Physics.Raycast(rayoFrontal, out hit, distanciaDetección) || Physics.Raycast(rayoTrasero, out hit, distanciaDetección))
        {
            
            if (hit.collider.tag == "box" || hit.collider.tag == "wall")
            {
                rigidbodyCaja.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;

            }
            else
            {
                rigidbodyCaja.constraints = ~RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
        }

    }

    private void ValidarMovimientoX()
    {

        RaycastHit hit;

        Ray rayoIzquierdo = new Ray(transform.position, Vector3.left);
        Ray rayoDerecho = new Ray(transform.position, Vector3.right);

        Debug.DrawRay(transform.position, Vector3.left * distanciaDetección, Color.red);
        Debug.DrawRay(transform.position, Vector3.right * distanciaDetección, Color.red);

        if (Physics.Raycast(rayoIzquierdo, out hit, distanciaDetección) || Physics.Raycast(rayoDerecho, out hit, distanciaDetección))
        {

            if (hit.collider.tag == "box" || hit.collider.tag == "wall")
            {
                rigidbodyCaja.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                rigidbodyCaja.constraints = ~RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "target")
        {
            levelManager.sumatoriaPuntosNivel++;
            Debug.Log("puntos "+levelManager.sumatoriaPuntosNivel);
            levelManager.ValidarVictoriaNivel();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "target")
        {
            levelManager.sumatoriaPuntosNivel--;
            Debug.Log("puntos "+levelManager.sumatoriaPuntosNivel);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Fondo")
        {
            levelManager.CargarDatos();
        }
    }
}
