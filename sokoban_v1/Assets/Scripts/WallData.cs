﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallData : MonoBehaviour
{

    public WallClass wallClass;
  

    // Start is called before the first frame update
    void Start()
    {

        wallClass = new WallClass(wallClass.tipoObjeto,wallClass.posicionX,wallClass.posicionY,wallClass.posicionZ);
        wallClass.tipoObjeto = "pared";
        wallClass.posicionX = gameObject.transform.position.x;
        wallClass.posicionY = gameObject.transform.position.y;
        wallClass.posicionZ = gameObject.transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string RetornarTipoObjeto() {
        return wallClass.tipoObjeto;
    }

    public float RetornarPosicionX()
    {
        return wallClass.posicionX;
    }

    public float RetornarPosicionY()
    {
        return wallClass.posicionY;
    }

    public float RetornarPosicionZ()
    {
        return wallClass.posicionZ;
    }
}
