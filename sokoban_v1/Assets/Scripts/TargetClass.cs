﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TargetClass 
{
    public string tipoObjeto;
    public float posicionX;
    public float posicionY;
    public float posicionZ;

    public TargetClass(string newTipoObjeto, float newPosicionX, float newPosicionY, float newPosicionZ) { 
        tipoObjeto = newTipoObjeto;
        posicionX = newPosicionX;
        posicionY = newPosicionY;
        posicionZ = newPosicionZ;
    }
}


